NeoLemmix Editor

by Stephan Neupert

This is an application to create new levels for NeoLemmix or modify old ones.

Compile informations:
- Build "NLEditor.sln" (e.g. with Visual Studio 2015 or newer)
- The editor requires C# 6.0 (or newer)
- There are no dependencies on external libraries

To build on Linux, install Mono and run xbuild without any arguments. 
Note that the editor has several known graphic issues on Linux, though fixing them
has currently low priority as the NeoLemmix player itself is Windows-only.

Any bugs, feature requests and support issues are currently handled via the lemmings forums at
  www.lemmingsforums.net
My nickname there is Nepster.  

Copyright: CC BY-NC 4.0 (2017)